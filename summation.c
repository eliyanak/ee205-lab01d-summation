///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Eliya Nakamura <eliyanak@hawaii.edu>
// @date  14 Jan 2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   // Accept an integer from n
   int n = atoi(argv[1]);

   // Initialize sum to 0
   int sum = 0;

   // Sum each number up to the integer n
   for (int i = 1; i <= n; i++){
      // Increment sum by the current value of i
      sum += i;
   }

   // Print result
   printf("%d\n", sum);

   return 0;
}
